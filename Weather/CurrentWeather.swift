//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by Tran Son on 12/20/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import Foundation

class CurrentWeather {
    let temperature: Double?
    let summary: String?
    let timezone: String?
    
    
    init(weatherDictionary: [String: Any]) {
        
        temperature = weatherDictionary["temperature"] as? Double
        summary = weatherDictionary["summary"] as? String
        timezone = weatherDictionary["timezone"] as? String
    }
    
}
