//
//  ForecastService.swift
//  WeatherApp
//
//  Created by Tran Son on 12/20/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import Foundation

class ForecastService {
    let forecastAPIKey: String
    let forecastBaseURL: URL?
    
    init(APIKey: String) {
        self.forecastAPIKey = APIKey
        self.forecastBaseURL = URL(string: "https://api.darksky.net/forecast/\(APIKey)")
        
    }
    
    func getForecast(latitude: Double, longtitude: Double, completion: @escaping (CurrentWeather?) -> Void) {
        
        if let forecastURL = URL(string: "\(forecastBaseURL!)/\(latitude),\(longtitude)") {
            
            let networkingProcessor = NetworkingProcessor(url: forecastURL)
            networkingProcessor.downloadJSONFromURL{ (jsonDictionary) in
                
                if let currentWeatherDictionary = jsonDictionary["currently"] as? [String: Any] {
                    let currentWeather = CurrentWeather(weatherDictionary: currentWeatherDictionary)
                    completion(currentWeather)
                }
                    
                else {
                    completion(nil)
                }
            }
        }
    }
}
