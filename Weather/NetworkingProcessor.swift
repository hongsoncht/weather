//
//  NetworkingProcessor.swift
//  WeatherApp
//
//  Created by Tran Son on 12/20/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//
import UIKit
import Foundation

class NetworkingProcessor {
    
    lazy var configuration: URLSessionConfiguration = URLSessionConfiguration.default
    lazy var session: URLSession = URLSession(configuration: self.configuration)
    
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
//    typealias JSONDictionaryHandler = (([String: Any]) -> Void)
    func downloadJSONFromURL(completion: @escaping JSONDictionaryHandler) {
        let request = URLRequest(url: self.url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            
            if error == nil {
                if let httpRespose = response as? HTTPURLResponse {
                    switch httpRespose.statusCode {
                    case 200:
                        if let data = data {
                            do {
                                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                                completion((jsonDictionary as? [String: Any])!)
                            }
                            catch let error as NSError{
                                print("Error processing json data: \(error.localizedDescription)")
                            }
                        }
                    default:
                        print("HTTP Respone Code: \(httpRespose.statusCode)")
                    }
                }
            }
            else {
                print("Error: \(error?.localizedDescription)")
            }
        }
        dataTask.resume()
    }
}

 typealias JSONDictionaryHandler = (([String: Any]) -> Void)
