//
//  ViewController.swift
//  Weather
//
//  Created by Tran Son on 12/21/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var summaryText: UILabel!
    @IBOutlet weak var temperatureText: UILabel!
    @IBOutlet weak var timezoneText: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let coordinate: (latitude: Double, longtitude: Double) = (37.8267,-122.4233)
        
    override func viewDidLoad() {
            super.viewDidLoad()
            
            let forecastService = ForecastService(APIKey: "2a70937716bb925b530b57751729728a")
            // loading khi đang truyền data
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = false
        
            forecastService.getForecast(latitude: coordinate.latitude, longtitude: coordinate.longtitude)
            { (currentWeather) in
                
                if let currentWeather = currentWeather {
                    
                    DispatchQueue.main.async {
                        
                        if let temperature = currentWeather.temperature {
                            self.temperatureText.text = "\(temperature)"
                        }
                        else {
                            self.temperatureText.text = "No data"
                            // tạo alert khi ko có data
                            self.alert(title: "NO DATA", message: "Không tìm thấy dữ liệu của temperature")
                        }
                        
                        if let summary = currentWeather.summary {
                            self.summaryText.text = "\(summary)"
                        }
                        else {
                            self.summaryText.text = "No data"
                            // tạo alert khi ko có data
                            self.alert(title: "NO DATA", message: "Không tìm thấy dữ liệu của summary")
                        }
                        
                        if let timezone = currentWeather.timezone {
                            self.timezoneText.text = "\(timezone)"
                        }
                        else {
                            self.timezoneText.text = "No data"
                            // tạo alert khi ko có data
                            self.alert(title: "NO DATA", message: "Không tìm thấy dữ liệu của timezone")
                        }
                        // ẩn đi sau khi load xong
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true

                    }
                }
            }
        }
    
    // tạo alert khi ko có dữ liệu
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
    
        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) in
        alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
    }
}

let forecastService = ForecastService(APIKey: "2a70937716bb925b530b57751729728a")
